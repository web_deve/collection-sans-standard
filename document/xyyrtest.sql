-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- 主机： 127.0.0.1
-- 生成日期： 2021-04-12 11:42:34
-- 服务器版本： 10.4.18-MariaDB
-- PHP 版本： 8.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 数据库： `xyyrtest`
--

-- --------------------------------------------------------

--
-- 表的结构 `goods`
--

CREATE TABLE `goods` (
  `id` int(4) UNSIGNED ZEROFILL NOT NULL,
  `icon` varchar(40) NOT NULL,
  `info` varchar(20) CHARACTER SET utf8 NOT NULL,
  `price` float NOT NULL,
  `stock` int(5) NOT NULL,
  `cato` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- 转存表中的数据 `goods`
--

INSERT INTO `goods` (`id`, `icon`, `info`, `price`, `stock`, `cato`) VALUES
(1001, '../images/yourname.jpg', 'Your Name', 15, 0, 'aceuil'),
(1002, '../images/nezha.jpg', 'Ne Zha', 26, 18, 'aceuil'),
(1003, '../images/totoro.jpg', 'TOTORO', 36, 14, 'aceuil'),
(1004, '../images/ciel.jpg', 'Castle in the Sky', 27, 15, 'aceuil'),
(1005, '../images/qian.jpg', 'Spirited Away', 26, 16, 'aceuil'),
(1006, '../images/robote.jpg', 'Robot Carnival', 16, 19, 'aceuil'),
(1007, '../images/orlacs.jpg', 'Orlacs Hand', 46, 18, 'europe'),
(1008, '../images/bigblue.jpg', 'The Big Blue', 26, 0, 'europe'),
(1009, '../images/homme.jpg', 'Man Who Sleeps', 28, 0, 'europe'),
(1010, '../images/turin.jpg', 'The Turin Horse', 21, 18, 'europe'),
(1011, '../images/valter.jpg', 'Valter', 23, 18, 'europe'),
(1012, '../images/tonghua.jpg', 'Jabberwocky', 6, 18, 'europe'),
(1013, '../images/qiefu.jpg', 'Harakiri', 36, 19, 'asia'),
(1014, '../images/borned.jpg', 'I Was Born', 17, 19, 'asia'),
(1015, '../images/yongxin.jpg', 'Yojimbo', 19, 16, 'asia'),
(1016, '../images/jidian.jpg', 'what time is it', 38, 17, 'asia'),
(1017, '../images/pengyou.jpg', 'Friend Home?', 26, 19, 'asia'),
(1018, '../images/huiyi.jpg', 'Memories', 31, 20, 'asia'),
(1019, '../images/sr.jpg', 'Sun Rise', 33, 16, 'american'),
(1020, '../images/sjr.jpg', 'Sherlock Jr.', 26, 0, 'american'),
(1021, '../images/af.jpg', 'Addams Family', 37, 19, 'american'),
(1022, '../images/dsl.jpg', 'Dr. Strangelove', 29, 16, 'american'),
(1023, '../images/yyss.jpg', 'Blade Runner', 24, 16, 'american'),
(1024, '../images/ryxq.jpg', 'Planet', 27, 18, 'american');

-- --------------------------------------------------------

--
-- 表的结构 `orders`
--

CREATE TABLE `orders` (
  `id` int(11) UNSIGNED ZEROFILL NOT NULL,
  `uid` varchar(20) NOT NULL,
  `orderinfo` varchar(5000) NOT NULL,
  `time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- 转存表中的数据 `orders`
--

INSERT INTO `orders` (`id`, `uid`, `orderinfo`, `time`) VALUES
(00000000001, '895890239@qq.com', '[{\"id\":\"1001\",\"icon\":\"..\\/images\\/yourname.jpg\",\"info\":\"Your Name\",\"price\":\"15\",\"stock\":\"12\",\"num\":2,\"sum\":30,\"deleted\":false},{\"id\":\"1002\",\"icon\":\"..\\/images\\/nezha.jpg\",\"info\":\"Ne Zha\",\"price\":\"26\",\"stock\":\"15\",\"num\":1,\"sum\":26,\"deleted\":false},{\"id\":\"1003\",\"icon\":\"..\\/images\\/totoro.jpg\",\"info\":\"TOTORO\",\"price\":\"36\",\"stock\":\"20\",\"num\":1,\"sum\":36,\"deleted\":false}]', '2021-04-11 18:11:48'),
(00000000002, '895890239@qq.com', '[{\"id\":\"1001\",\"icon\":\"..\\/images\\/yourname.jpg\",\"info\":\"Your Name\",\"price\":\"15\",\"stock\":\"10\",\"num\":1,\"sum\":\"15\",\"deleted\":false},{\"id\":\"1002\",\"icon\":\"..\\/images\\/nezha.jpg\",\"info\":\"Ne Zha\",\"price\":\"26\",\"stock\":\"14\",\"num\":1,\"sum\":\"26\",\"deleted\":false},{\"id\":\"1003\",\"icon\":\"..\\/images\\/totoro.jpg\",\"info\":\"TOTORO\",\"price\":\"36\",\"stock\":\"19\",\"num\":1,\"sum\":\"36\",\"deleted\":false},{\"id\":\"1006\",\"icon\":\"..\\/images\\/robote.jpg\",\"info\":\"Robot Carnival\",\"price\":\"16\",\"stock\":\"19\",\"num\":1,\"sum\":\"16\",\"deleted\":false},{\"id\":\"1005\",\"icon\":\"..\\/images\\/qian.jpg\",\"info\":\"Spirited Away\",\"price\":\"26\",\"stock\":\"15\",\"num\":1,\"sum\":\"26\",\"deleted\":false},{\"id\":\"1004\",\"icon\":\"..\\/images\\/ciel.jpg\",\"info\":\"Castle in the Sky\",\"price\":\"27\",\"stock\":\"19\",\"num\":1,\"sum\":\"27\",\"deleted\":false}]', '2021-04-11 18:15:09'),
(00000000003, '895890239@qq.com', '[{\"id\":\"1001\",\"icon\":\"..\\/images\\/yourname.jpg\",\"info\":\"Your Name\",\"price\":\"15\",\"stock\":\"10\",\"num\":1,\"sum\":\"15\",\"deleted\":false},{\"id\":\"1002\",\"icon\":\"..\\/images\\/nezha.jpg\",\"info\":\"Ne Zha\",\"price\":\"26\",\"stock\":\"14\",\"num\":1,\"sum\":\"26\",\"deleted\":false},{\"id\":\"1003\",\"icon\":\"..\\/images\\/totoro.jpg\",\"info\":\"TOTORO\",\"price\":\"36\",\"stock\":\"19\",\"num\":1,\"sum\":\"36\",\"deleted\":false},{\"id\":\"1006\",\"icon\":\"..\\/images\\/robote.jpg\",\"info\":\"Robot Carnival\",\"price\":\"16\",\"stock\":\"19\",\"num\":1,\"sum\":\"16\",\"deleted\":false},{\"id\":\"1005\",\"icon\":\"..\\/images\\/qian.jpg\",\"info\":\"Spirited Away\",\"price\":\"26\",\"stock\":\"15\",\"num\":1,\"sum\":\"26\",\"deleted\":false},{\"id\":\"1004\",\"icon\":\"..\\/images\\/ciel.jpg\",\"info\":\"Castle in the Sky\",\"price\":\"27\",\"stock\":\"19\",\"num\":1,\"sum\":\"27\",\"deleted\":false}]', '2021-04-11 18:16:39'),
(00000000004, '895890239@qq.com', '[{\"id\":\"1001\",\"icon\":\"..\\/images\\/yourname.jpg\",\"info\":\"Your Name\",\"price\":\"15\",\"stock\":\"10\",\"num\":1,\"sum\":\"15\",\"deleted\":false},{\"id\":\"1002\",\"icon\":\"..\\/images\\/nezha.jpg\",\"info\":\"Ne Zha\",\"price\":\"26\",\"stock\":\"14\",\"num\":1,\"sum\":\"26\",\"deleted\":false},{\"id\":\"1003\",\"icon\":\"..\\/images\\/totoro.jpg\",\"info\":\"TOTORO\",\"price\":\"36\",\"stock\":\"19\",\"num\":1,\"sum\":\"36\",\"deleted\":false},{\"id\":\"1006\",\"icon\":\"..\\/images\\/robote.jpg\",\"info\":\"Robot Carnival\",\"price\":\"16\",\"stock\":\"19\",\"num\":1,\"sum\":\"16\",\"deleted\":false},{\"id\":\"1005\",\"icon\":\"..\\/images\\/qian.jpg\",\"info\":\"Spirited Away\",\"price\":\"26\",\"stock\":\"15\",\"num\":1,\"sum\":\"26\",\"deleted\":false},{\"id\":\"1004\",\"icon\":\"..\\/images\\/ciel.jpg\",\"info\":\"Castle in the Sky\",\"price\":\"27\",\"stock\":\"19\",\"num\":1,\"sum\":\"27\",\"deleted\":false},{\"id\":\"1009\",\"icon\":\"..\\/images\\/homme.jpg\",\"info\":\"Man Who Sleeps\",\"price\":\"28\",\"stock\":\"19\",\"num\":1,\"sum\":\"28\",\"deleted\":false},{\"id\":\"1010\",\"icon\":\"..\\/images\\/turin.jpg\",\"info\":\"The Turin Horse\",\"price\":\"21\",\"stock\":\"19\",\"num\":1,\"sum\":\"21\",\"deleted\":false},{\"id\":\"1011\",\"icon\":\"..\\/images\\/valter.jpg\",\"info\":\"Valter\",\"price\":\"23\",\"stock\":\"19\",\"num\":1,\"sum\":\"23\",\"deleted\":false}]', '2021-04-11 18:17:13'),
(00000000005, '895890239@qq.com', '[{\"id\":\"1001\",\"icon\":\"..\\/images\\/yourname.jpg\",\"info\":\"Your Name\",\"price\":\"15\",\"stock\":\"10\",\"num\":1,\"sum\":\"15\",\"deleted\":false},{\"id\":\"1002\",\"icon\":\"..\\/images\\/nezha.jpg\",\"info\":\"Ne Zha\",\"price\":\"26\",\"stock\":\"14\",\"num\":1,\"sum\":\"26\",\"deleted\":false},{\"id\":\"1003\",\"icon\":\"..\\/images\\/totoro.jpg\",\"info\":\"TOTORO\",\"price\":\"36\",\"stock\":\"19\",\"num\":1,\"sum\":\"36\",\"deleted\":false},{\"id\":\"1006\",\"icon\":\"..\\/images\\/robote.jpg\",\"info\":\"Robot Carnival\",\"price\":\"16\",\"stock\":\"19\",\"num\":1,\"sum\":\"16\",\"deleted\":false},{\"id\":\"1005\",\"icon\":\"..\\/images\\/qian.jpg\",\"info\":\"Spirited Away\",\"price\":\"26\",\"stock\":\"15\",\"num\":1,\"sum\":\"26\",\"deleted\":false},{\"id\":\"1004\",\"icon\":\"..\\/images\\/ciel.jpg\",\"info\":\"Castle in the Sky\",\"price\":\"27\",\"stock\":\"19\",\"num\":1,\"sum\":\"27\",\"deleted\":false},{\"id\":\"1009\",\"icon\":\"..\\/images\\/homme.jpg\",\"info\":\"Man Who Sleeps\",\"price\":\"28\",\"stock\":\"20\",\"num\":1,\"sum\":\"28\",\"deleted\":false},{\"id\":\"1010\",\"icon\":\"..\\/images\\/turin.jpg\",\"info\":\"The Turin Horse\",\"price\":\"21\",\"stock\":\"20\",\"num\":1,\"sum\":\"21\",\"deleted\":false},{\"id\":\"1011\",\"icon\":\"..\\/images\\/valter.jpg\",\"info\":\"Valter\",\"price\":\"23\",\"stock\":\"20\",\"num\":1,\"sum\":\"23\",\"deleted\":false}]', '2021-04-11 18:26:17'),
(00000000006, '895890239@qq.com', '[{\"id\":\"1001\",\"icon\":\"..\\/images\\/yourname.jpg\",\"info\":\"Your Name\",\"price\":\"15\",\"stock\":\"9\",\"num\":3,\"sum\":45,\"deleted\":false},{\"id\":\"1002\",\"icon\":\"..\\/images\\/nezha.jpg\",\"info\":\"Ne Zha\",\"price\":\"26\",\"stock\":\"13\",\"num\":1,\"sum\":26,\"deleted\":false},{\"id\":\"1003\",\"icon\":\"..\\/images\\/totoro.jpg\",\"info\":\"TOTORO\",\"price\":\"36\",\"stock\":\"18\",\"num\":1,\"sum\":36,\"deleted\":false}]', '2021-04-11 18:28:44'),
(00000000007, '895890239@qq.com', '[{\"id\":\"1001\",\"icon\":\"..\\/images\\/yourname.jpg\",\"info\":\"Your Name\",\"price\":\"15\",\"stock\":\"5\",\"num\":3,\"sum\":45,\"deleted\":false},{\"id\":\"1002\",\"icon\":\"..\\/images\\/nezha.jpg\",\"info\":\"Ne Zha\",\"price\":\"26\",\"stock\":\"12\",\"num\":1,\"sum\":26,\"deleted\":false}]', '2021-04-11 18:42:16'),
(00000000008, '895890239@qq.com', '[{\"id\":\"1005\",\"icon\":\"..\\/images\\/qian.jpg\",\"info\":\"Spirited Away\",\"price\":\"26\",\"stock\":\"14\",\"num\":1,\"sum\":\"26\",\"deleted\":false},{\"id\":\"1003\",\"icon\":\"..\\/images\\/totoro.jpg\",\"info\":\"TOTORO\",\"price\":\"36\",\"stock\":\"17\",\"num\":1,\"sum\":\"36\",\"deleted\":false},{\"id\":\"1004\",\"icon\":\"..\\/images\\/ciel.jpg\",\"info\":\"Castle in the Sky\",\"price\":\"27\",\"stock\":\"18\",\"num\":1,\"sum\":\"27\",\"deleted\":false}]', '2021-04-11 18:57:56'),
(00000000009, '895890239@qq.com', '[{\"id\":\"1004\",\"icon\":\"..\\/images\\/ciel.jpg\",\"info\":\"Castle in the Sky\",\"price\":\"27\",\"stock\":\"18\",\"num\":1,\"sum\":\"27\",\"deleted\":false},{\"id\":\"1006\",\"icon\":\"..\\/images\\/robote.jpg\",\"info\":\"Robot Carnival\",\"price\":\"16\",\"stock\":\"17\",\"num\":1,\"sum\":\"16\",\"deleted\":false}]', '2021-04-11 18:58:07'),
(00000000010, '895890239@qq.com', '[{\"id\":\"1002\",\"icon\":\"..\\/images\\/nezha.jpg\",\"info\":\"Ne Zha\",\"price\":\"26\",\"stock\":\"11\",\"num\":1,\"sum\":\"26\",\"deleted\":false},{\"id\":\"1003\",\"icon\":\"..\\/images\\/totoro.jpg\",\"info\":\"TOTORO\",\"price\":\"36\",\"stock\":\"17\",\"num\":1,\"sum\":\"36\",\"deleted\":false}]', '2021-04-11 18:58:25'),
(00000000011, '895890239@qq.com', '[{\"id\":\"1003\",\"icon\":\"..\\/images\\/totoro.jpg\",\"info\":\"TOTORO\",\"price\":\"36\",\"stock\":\"17\",\"num\":1,\"sum\":\"36\",\"deleted\":false},{\"id\":\"1005\",\"icon\":\"..\\/images\\/qian.jpg\",\"info\":\"Spirited Away\",\"price\":\"26\",\"stock\":\"13\",\"num\":1,\"sum\":\"26\",\"deleted\":false}]', '2021-04-11 18:58:39'),
(00000000012, '895890239@qq.com', '[{\"id\":\"1004\",\"icon\":\"..\\/images\\/ciel.jpg\",\"info\":\"Castle in the Sky\",\"price\":\"27\",\"stock\":\"17\",\"num\":1,\"sum\":\"27\",\"deleted\":false},{\"id\":\"1006\",\"icon\":\"..\\/images\\/robote.jpg\",\"info\":\"Robot Carnival\",\"price\":\"16\",\"stock\":\"16\",\"num\":1,\"sum\":\"16\",\"deleted\":false}]', '2021-04-11 18:59:11'),
(00000000013, '895890239@qq.com', '[{\"id\":\"1004\",\"icon\":\"..\\/images\\/ciel.jpg\",\"info\":\"Castle in the Sky\",\"price\":\"27\",\"stock\":\"17\",\"num\":1,\"sum\":\"27\",\"deleted\":false},{\"id\":\"1006\",\"icon\":\"..\\/images\\/robote.jpg\",\"info\":\"Robot Carnival\",\"price\":\"16\",\"stock\":\"16\",\"num\":1,\"sum\":\"16\",\"deleted\":false}]', '2021-04-11 18:59:12'),
(00000000014, '895890239@qq.com', '[{\"id\":\"1002\",\"icon\":\"..\\/images\\/nezha.jpg\",\"info\":\"Ne Zha\",\"price\":\"26\",\"stock\":\"10\",\"num\":1,\"sum\":\"26\",\"deleted\":false},{\"id\":\"1003\",\"icon\":\"..\\/images\\/totoro.jpg\",\"info\":\"TOTORO\",\"price\":\"36\",\"stock\":\"16\",\"num\":1,\"sum\":\"36\",\"deleted\":false}]', '2021-04-11 18:59:51'),
(00000000015, '895890239@qq.com', '[{\"id\":\"1003\",\"icon\":\"..\\/images\\/totoro.jpg\",\"info\":\"TOTORO\",\"price\":\"36\",\"stock\":\"15\",\"num\":6,\"sum\":216,\"deleted\":false},{\"id\":\"1004\",\"icon\":\"..\\/images\\/ciel.jpg\",\"info\":\"Castle in the Sky\",\"price\":\"27\",\"stock\":\"17\",\"num\":8,\"sum\":216,\"deleted\":false}]', '2021-04-11 19:00:38'),
(00000000016, '895890239@qq.com', '[{\"id\":\"1004\",\"icon\":\"..\\/images\\/ciel.jpg\",\"info\":\"Castle in the Sky\",\"price\":\"27\",\"stock\":\"17\",\"num\":8,\"sum\":216,\"deleted\":false},{\"id\":\"1005\",\"icon\":\"..\\/images\\/qian.jpg\",\"info\":\"Spirited Away\",\"price\":\"26\",\"stock\":\"12\",\"num\":3,\"sum\":78,\"deleted\":false}]', '2021-04-11 19:01:17'),
(00000000017, '895890239@qq.com', '[{\"id\":\"1001\",\"icon\":\"..\\/images\\/yourname.jpg\",\"info\":\"Your Name\",\"price\":\"15\",\"stock\":\"19\",\"num\":1,\"sum\":\"15\",\"deleted\":false},{\"id\":\"1002\",\"icon\":\"..\\/images\\/nezha.jpg\",\"info\":\"Ne Zha\",\"price\":\"26\",\"stock\":\"19\",\"num\":1,\"sum\":\"26\",\"deleted\":false},{\"id\":\"1003\",\"icon\":\"..\\/images\\/totoro.jpg\",\"info\":\"TOTORO\",\"price\":\"36\",\"stock\":\"19\",\"num\":1,\"sum\":\"36\",\"deleted\":false}]', '2021-04-12 06:20:14'),
(00000000018, '895890239@qq.com', '[{\"id\":\"1001\",\"icon\":\"..\\/images\\/yourname.jpg\",\"info\":\"Your Name\",\"price\":\"15\",\"stock\":\"19\",\"num\":2,\"sum\":30,\"deleted\":false},{\"id\":\"1002\",\"icon\":\"..\\/images\\/nezha.jpg\",\"info\":\"Ne Zha\",\"price\":\"26\",\"stock\":\"19\",\"num\":1,\"sum\":26,\"deleted\":false},{\"id\":\"1004\",\"icon\":\"..\\/images\\/ciel.jpg\",\"info\":\"Castle in the Sky\",\"price\":\"27\",\"stock\":\"19\",\"num\":1,\"sum\":27,\"deleted\":false}]', '2021-04-12 06:25:32'),
(00000000019, '895890239@qq.com', '[{\"id\":\"1001\",\"icon\":\"..\\/images\\/yourname.jpg\",\"info\":\"Your Name\",\"price\":\"15\",\"stock\":\"19\",\"num\":1,\"sum\":15,\"deleted\":false},{\"id\":\"1002\",\"icon\":\"..\\/images\\/nezha.jpg\",\"info\":\"Ne Zha\",\"price\":\"26\",\"stock\":\"19\",\"num\":2,\"sum\":52,\"deleted\":false},{\"id\":\"1003\",\"icon\":\"..\\/images\\/totoro.jpg\",\"info\":\"TOTORO\",\"price\":\"36\",\"stock\":\"19\",\"num\":1,\"sum\":36,\"deleted\":false},{\"id\":\"1006\",\"icon\":\"..\\/images\\/robote.jpg\",\"info\":\"Robot Carnival\",\"price\":\"16\",\"stock\":\"19\",\"num\":1,\"sum\":16,\"deleted\":false}]', '2021-04-12 06:26:21'),
(00000000020, '895890239@qq.com', '[{\"id\":\"1001\",\"icon\":\"..\\/images\\/yourname.jpg\",\"info\":\"Your Name\",\"price\":\"15\",\"stock\":\"18\",\"num\":1,\"sum\":\"15\",\"deleted\":false}]', '2021-04-12 06:58:52'),
(00000000021, '895890239@qq.com', '[{\"id\":\"1001\",\"icon\":\"..\\/images\\/yourname.jpg\",\"info\":\"Your Name\",\"price\":\"15\",\"stock\":\"17\",\"num\":1,\"sum\":\"15\",\"deleted\":false},{\"id\":\"1002\",\"icon\":\"..\\/images\\/nezha.jpg\",\"info\":\"Ne Zha\",\"price\":\"26\",\"stock\":\"16\",\"num\":1,\"sum\":\"26\",\"deleted\":false}]', '2021-04-12 06:59:28'),
(00000000022, '895890239@qq.com', '[{\"id\":\"1004\",\"icon\":\"..\\/images\\/ciel.jpg\",\"info\":\"Castle in the Sky\",\"price\":\"27\",\"stock\":\"19\",\"num\":2,\"sum\":54,\"deleted\":false},{\"id\":\"1005\",\"icon\":\"..\\/images\\/qian.jpg\",\"info\":\"Spirited Away\",\"price\":\"26\",\"stock\":\"19\",\"num\":1,\"sum\":26,\"deleted\":false}]', '2021-04-12 06:59:47'),
(00000000023, '895890239@qq.com', '[{\"id\":\"1001\",\"icon\":\"..\\/images\\/yourname.jpg\",\"info\":\"Your Name\",\"price\":\"15\",\"stock\":20,\"num\":20,\"sum\":300,\"deleted\":false},{\"id\":\"1002\",\"icon\":\"..\\/images\\/nezha.jpg\",\"info\":\"Ne Zha\",\"price\":\"26\",\"stock\":20,\"num\":1,\"sum\":26,\"deleted\":false}]', '2021-04-12 07:14:57'),
(00000000024, '895890239@qq.com', '[{\"id\":\"1004\",\"icon\":\"..\\/images\\/ciel.jpg\",\"info\":\"Castle in the Sky\",\"price\":\"27\",\"stock\":20,\"num\":5,\"sum\":135,\"deleted\":false},{\"id\":\"1003\",\"icon\":\"..\\/images\\/totoro.jpg\",\"info\":\"TOTORO\",\"price\":\"36\",\"stock\":20,\"num\":5,\"sum\":180,\"deleted\":false},{\"id\":\"1005\",\"icon\":\"..\\/images\\/qian.jpg\",\"info\":\"Spirited Away\",\"price\":\"26\",\"stock\":20,\"num\":4,\"sum\":104,\"deleted\":false},{\"id\":\"1006\",\"icon\":\"..\\/images\\/robote.jpg\",\"info\":\"Robot Carnival\",\"price\":\"16\",\"stock\":20,\"num\":1,\"sum\":16,\"deleted\":false}]', '2021-04-12 07:17:22'),
(00000000025, '895890239@qq.com', '[{\"id\":\"1008\",\"icon\":\"..\\/images\\/bigblue.jpg\",\"info\":\"The Big Blue\",\"price\":\"26\",\"stock\":19,\"num\":18,\"sum\":468,\"deleted\":false},{\"id\":\"1007\",\"icon\":\"..\\/images\\/orlacs.jpg\",\"info\":\"Orlacs Hand\",\"price\":\"46\",\"stock\":19,\"num\":1,\"sum\":46,\"deleted\":false},{\"id\":\"1009\",\"icon\":\"..\\/images\\/homme.jpg\",\"info\":\"Man Who Sleeps\",\"price\":\"28\",\"stock\":19,\"num\":1,\"sum\":28,\"deleted\":false},{\"id\":\"1010\",\"icon\":\"..\\/images\\/turin.jpg\",\"info\":\"The Turin Horse\",\"price\":\"21\",\"stock\":19,\"num\":1,\"sum\":21,\"deleted\":false},{\"id\":\"1011\",\"icon\":\"..\\/images\\/valter.jpg\",\"info\":\"Valter\",\"price\":\"23\",\"stock\":19,\"num\":1,\"sum\":23,\"deleted\":false},{\"id\":\"1012\",\"icon\":\"..\\/images\\/tonghua.jpg\",\"info\":\"Jabberwocky\",\"price\":\"6\",\"stock\":19,\"num\":1,\"sum\":6,\"deleted\":false}]', '2021-04-12 07:24:50'),
(00000000026, '895890239@qq.com', '[{\"id\":\"1009\",\"icon\":\"..\\/images\\/homme.jpg\",\"info\":\"Man Who Sleeps\",\"price\":\"28\",\"stock\":18,\"num\":18,\"sum\":504,\"deleted\":false}]', '2021-04-12 07:25:18'),
(00000000027, '895890239@qq.com', '[{\"id\":\"1020\",\"icon\":\"..\\/images\\/sjr.jpg\",\"info\":\"Sherlock Jr.\",\"price\":\"26\",\"stock\":20,\"num\":20,\"sum\":520,\"deleted\":false}]', '2021-04-12 07:25:40'),
(00000000028, '895890239@qq.com', '[{\"id\":\"1019\",\"icon\":\"..\\/images\\/sr.jpg\",\"info\":\"Sun Rise\",\"price\":\"33\",\"stock\":20,\"num\":1,\"sum\":\"33\",\"deleted\":false},{\"id\":\"1021\",\"icon\":\"..\\/images\\/af.jpg\",\"info\":\"Addams Family\",\"price\":\"37\",\"stock\":20,\"num\":1,\"sum\":\"37\",\"deleted\":false},{\"id\":\"1024\",\"icon\":\"..\\/images\\/ryxq.jpg\",\"info\":\"Planet\",\"price\":\"27\",\"stock\":19,\"num\":1,\"sum\":\"27\",\"deleted\":false},{\"id\":\"1023\",\"icon\":\"..\\/images\\/yyss.jpg\",\"info\":\"Blade Runner\",\"price\":\"24\",\"stock\":19,\"num\":1,\"sum\":\"24\",\"deleted\":false},{\"id\":\"1022\",\"icon\":\"..\\/images\\/dsl.jpg\",\"info\":\"Dr. Strangelove\",\"price\":\"29\",\"stock\":19,\"num\":1,\"sum\":\"29\",\"deleted\":false},{\"id\":\"1014\",\"icon\":\"..\\/images\\/borned.jpg\",\"info\":\"I Was Born\",\"price\":\"17\",\"stock\":20,\"num\":1,\"sum\":\"17\",\"deleted\":false},{\"id\":\"1013\",\"icon\":\"..\\/images\\/qiefu.jpg\",\"info\":\"Harakiri\",\"price\":\"36\",\"stock\":20,\"num\":1,\"sum\":\"36\",\"deleted\":false},{\"id\":\"1015\",\"icon\":\"..\\/images\\/yongxin.jpg\",\"info\":\"Yojimbo\",\"price\":\"19\",\"stock\":20,\"num\":1,\"sum\":\"19\",\"deleted\":false},{\"id\":\"1016\",\"icon\":\"..\\/images\\/jidian.jpg\",\"info\":\"what time is it\",\"price\":\"38\",\"stock\":20,\"num\":1,\"sum\":\"38\",\"deleted\":false},{\"id\":\"1008\",\"icon\":\"..\\/images\\/bigblue.jpg\",\"info\":\"The Big Blue\",\"price\":\"26\",\"stock\":1,\"num\":1,\"sum\":\"26\",\"deleted\":false}]', '2021-04-12 07:26:31'),
(00000000029, '895890239@qq.com', '[{\"id\":\"1015\",\"icon\":\"..\\/images\\/yongxin.jpg\",\"info\":\"Yojimbo\",\"price\":\"19\",\"stock\":20,\"num\":3,\"sum\":57,\"deleted\":false},{\"id\":\"1016\",\"icon\":\"..\\/images\\/jidian.jpg\",\"info\":\"what time is it\",\"price\":\"38\",\"stock\":20,\"num\":1,\"sum\":38,\"deleted\":false}]', '2021-04-12 08:01:55'),
(00000000030, '895890239@qq.com', '[{\"id\":\"1017\",\"icon\":\"..\\/images\\/pengyou.jpg\",\"info\":\"Friend Home?\",\"price\":\"26\",\"stock\":20,\"num\":1,\"sum\":\"26\",\"deleted\":false},{\"id\":\"1016\",\"icon\":\"..\\/images\\/jidian.jpg\",\"info\":\"what time is it\",\"price\":\"38\",\"stock\":18,\"num\":1,\"sum\":\"38\",\"deleted\":false},{\"id\":\"1015\",\"icon\":\"..\\/images\\/yongxin.jpg\",\"info\":\"Yojimbo\",\"price\":\"19\",\"stock\":17,\"num\":1,\"sum\":\"19\",\"deleted\":false}]', '2021-04-12 08:02:41'),
(00000000031, '895890239@qq.com', '[{\"id\":\"1023\",\"icon\":\"..\\/images\\/yyss.jpg\",\"info\":\"Blade Runner\",\"price\":\"24\",\"stock\":18,\"num\":2,\"sum\":48,\"deleted\":false}]', '2021-04-12 08:03:06'),
(00000000032, '895890239@qq.com', '[{\"id\":\"1022\",\"icon\":\"..\\/images\\/dsl.jpg\",\"info\":\"Dr. Strangelove\",\"price\":\"29\",\"stock\":18,\"num\":2,\"sum\":58,\"deleted\":false}]', '2021-04-12 08:03:22'),
(00000000033, '895890239@qq.com', '[{\"id\":\"1019\",\"icon\":\"..\\/images\\/sr.jpg\",\"info\":\"Sun Rise\",\"price\":\"33\",\"stock\":19,\"num\":3,\"sum\":99,\"deleted\":false}]', '2021-04-12 08:03:37'),
(00000000034, '895890239@qq.com', '[{\"id\":\"1002\",\"icon\":\"..\\/images\\/nezha.jpg\",\"info\":\"Ne Zha\",\"price\":\"26\",\"stock\":19,\"num\":1,\"sum\":\"26\",\"deleted\":false},{\"id\":\"1003\",\"icon\":\"..\\/images\\/totoro.jpg\",\"info\":\"TOTORO\",\"price\":\"36\",\"stock\":15,\"num\":1,\"sum\":\"36\",\"deleted\":false}]', '2021-04-12 08:35:22');

-- --------------------------------------------------------

--
-- 表的结构 `users`
--

CREATE TABLE `users` (
  `uid` int(4) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `email` varchar(20) NOT NULL,
  `password` varchar(50) NOT NULL,
  `gender` char(1) NOT NULL,
  `metier` varchar(10) NOT NULL,
  `naissance` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- 转存表中的数据 `users`
--

INSERT INTO `users` (`uid`, `name`, `email`, `password`, `gender`, `metier`, `naissance`) VALUES
(1, 'YangWang', 'xyyr190304@gmail.com', '123456', 'M', 'etudiant', '1995-01-15'),
(2, 'DusanMakavejev', '895890239@qq.com', '789456', 'M', 'professeur', '1995-01-15'),
(16, 'KmFl', '2602683000@qq.com', '741852', 'M', 'etudiant', '2021-03-11'),
(19, 'JanSd', '165161@163.com', '963258741', 'F', 'etudiant', '2021-03-10'),
(21, 'XyyrYeqsd', '123456@dqs.com', 'qsdfzefqsd', 'M', 'etudiant', '2021-03-18'),
(22, 'hlkknszedgty', '1234565@4534.com', 'tdutdtdt', 'M', 'etudiant', '2021-03-05'),
(23, 'XingningLU', 'luxingningfr@gmail.c', '123456', 'F', 'etudiant', '1998-08-10');

--
-- 转储表的索引
--

--
-- 表的索引 `goods`
--
ALTER TABLE `goods`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`uid`);

--
-- 在导出的表使用AUTO_INCREMENT
--

--
-- 使用表AUTO_INCREMENT `goods`
--
ALTER TABLE `goods`
  MODIFY `id` int(4) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1025;

--
-- 使用表AUTO_INCREMENT `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- 使用表AUTO_INCREMENT `users`
--
ALTER TABLE `users`
  MODIFY `uid` int(4) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
