﻿/**
 * Commodity item class
 * */
class GoodsItem {
    /**
     * Constructor, requires attribute information and parent node
     * @param {any} _propose
     * @param {any} parent
     */
    constructor(_propose, parent) {
        this.propose = _propose;
        this.goods = this.initGoods(parent);
        this.render(_propose);
    }
    /**
     * Build html structure
     * @param {any} parent
     */
    initGoods(parent) {
        if (this.goods == true)
            return this.goods;

        let div = document.createElement("div");

        Object.assign(div.style, {
            display: "inline-block",
            position: "relative",
            marginLeft: "10px",
            height: "290px",
            width: "180px"
        });
        this.createImgitem(div);

        this.createGoodprice(div);

        this.createStock(div);

        parent.appendChild(div);
        return div;
    }
    /**
     * Picture content filling
     * @param {any} parent
     */
    createImgitem(parent) {
        let div = document.createElement("div");
        Object.assign(div.style, {
            textAlign: "center",
            height: "240px",
            width: "100 %"
        });
        this.goodsimg = new Image();
        Object.assign(this.goodsimg, {
            width: "140", height: "210"
        });
        this.goodsinfo = document.createElement("p");
        Object.assign(this.goodsinfo.style, {
            position: "relative",
            top: "-5px",
            fontSize: "14px"
        });
        this.goodsimg.addEventListener("click", this.imgclickHandler);
        div.appendChild(this.goodsimg);
        div.appendChild(this.goodsinfo);
        parent.appendChild(div);
    }
    /**
     * Price content filling
     * @param {any} parent
     */
    createGoodprice(parent) {

        let div = document.createElement("div");
        Object.assign(div.style, {
            border: "2px solid #e01222",
            height: "22px"
        });
        let sp = document.createElement("span");
        this.price = document.createElement("div");
        this.price.style.display = "inline";
        this.divbn = document.createElement("div");
        Object.assign(this.divbn.style, {
            width: "149px",
            height: "24px",
            position: "absolute",
            right: "1px",
            cursor:"pointer",
            backgroundColor: "orangered",
            top: "241px",
            textAlign: "center",
            color: "white"
        });
        sp.textContent = "$";
        this.divbn.textContent = "ajouter au panier";
        this.divbn.addEventListener("mouseenter", this.mouseHandler);
        this.divbn.addEventListener("mouseleave", this.mouseHandler);
        this.divbn.addEventListener("click", this.clickHandler);
        this.divbn.self = this;
        div.appendChild(sp);
        div.appendChild(this.price);
        div.appendChild(this.divbn);

        parent.appendChild(div);

    }
    /**
     * stock content filling
     * @param {any} parent
     */
    createStock(parent) {

        let div = document.createElement("div");

        this.stock = document.createElement("span");
        this.stock.id = "0"+this.propose.id;

        Object.assign(div.style, {
            height: "24px",
            width: "100%",
            display:"none",
            color: "white",
            background: "linear-gradient(to left top, red, black)",
            textAlign: "center",
            position: "absolute",
            right: 0
        });
        div.innerHTML = "En Stock:";
        div.appendChild(this.stock);
        parent.appendChild(div);

    }
    /**
     * Event: Mouse move to show inventory
     * @param {any} e
     */
    mouseHandler(e) {
        let userid = getCookie("useremail");
        if (userid == "895890239@qq.com") {
            if (e.type === "mouseenter") {
                this.parentNode.parentNode.lastElementChild.style.display = "block";
            }
            else if (e.type === "mouseleave") {
                this.parentNode.parentNode.lastElementChild.style.display = "none";
            }
        }
    }
    /**
     * Event: add shopping cart
     * @param {any} e
     */
    clickHandler(e) {
        //  console.log(this.self);
        console.log("oncliic");
        let data = this.self.propose;
        console.log(data.stock);
        if (Number(data.stock) == 0)
            return;
        let arrcookie = getCookie_in_arr("sitem");
        console.log(this.self.propose.id);
        for (let i = 0; i < arrcookie.length; i++)
            if (this.self.propose.id == arrcookie[i]) {
                console.log(this.self.propose);
                console.log(arrcookie[i]);
                break;
            }
        let evt = new Event(GoodsItem.ADD_SHOPPING_LIST_EVENT);
        evt.data = data;
        document.dispatchEvent(evt);
        let xhr = new XMLHttpRequest();
        xhr.open("GET", "http://wy.test/php/wjson.php?id=" + this.self.propose.id + "&stock=" + (this.self.propose.stock - 1), true); //change the data of json file
        xhr.onload = function () {                                                    //if success,creat an event 
            if (this.response) {

                let evt = new Event("datachange_event");
                evt.data = data;
                console.log(evt.data.id);
                console.log("data_change");
                document.dispatchEvent(evt);
            }
        }
        xhr.send();

    

    }
    /*imgclickHandler(e) {
        console.log("img");
        window.location.href = "../.html/goodsinfo.html?id=" + this.id + "&time=" + new Date().getTime();
    }*/
    /**
     * Get attributes
     * @param {any} _propose
     */
    render(_propose) {
        this.goodsimg.src = _propose.icon;
        this.goodsimg.id = _propose.id;
        this.goodsinfo.textContent = _propose.info;
        this.price.textContent = _propose.price;
        this.stock.textContent = _propose.stock;
        this.divbn.id = _propose.id;

    }

    static get ADD_SHOPPING_LIST_EVENT() {
        return "add_shopping_list_event";
    }

    
}
