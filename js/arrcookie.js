const getCookie = (name) => {
    if (document.cookie.length > 0) {
        let start = document.cookie.indexOf(name + "=")
        if (start != -1) {
            start = start + name.length + 1;
            let end = document.cookie.indexOf(";", start);
            if (end == -1) end = document.cookie.length
            return unescape(document.cookie.substring(start, end))
        }
    }
    return null;
}
const unique = (arr) => {
   // console.log(arr);
    // arr.pop();
    var tmp = new Array();
    for (var i in arr) {
       // console.log(tmp);
        if (tmp.indexOf(arr[i]) === -1) {
            tmp.push(arr[i]);

        }
     
    }
    return tmp;
}
const setCookie = (name, value) => {
 
    const cookieObj = [];
    if (getCookie(name) !== null) {
        if (getCookie(name).split(',').length >= 1) {
            for (let i = 0; i < getCookie(name).split(',').length; i++) {
                cookieObj.push(getCookie(name).split(',')[i]);
            }
        } else {
            cookieObj.push(getCookie(name).split(','));
        }

    }
    let arr = unique(cookieObj);
    if (arr.indexOf(value) === -1)
        arr.push(value);


    document.cookie = name + '=' + arr + ';path=/';
}

const getCookie_in_arr = (name, value) => {

    const cookieObj = [];
    if (getCookie(name) !== null) {
        if (getCookie(name).split(',').length >= 1) {
            for (let i = 0; i < getCookie(name).split(',').length; i++) {
                cookieObj.push(getCookie(name).split(',')[i]);
            }
        } else {
            cookieObj.push(getCookie(name).split(','));
        }

    }
    let arr = unique(cookieObj);
    return arr;
}


const deCookie=(name, value)=>{
    const cookieObj = [];
    if (getCookie(name) !== null) {
        if (getCookie(name).split(',').length >= 1) {
            for (let i = 0; i < getCookie(name).split(',').length; i++) {
                cookieObj.push(getCookie(name).split(',')[i]);
            }
        } else {
            cookieObj.push(getCookie(name).split(','));
        }

    }


    if (cookieObj.indexOf(value) === -1)
        return;
    else {
        cookieObj.splice(cookieObj.indexOf(value), 1);
    }
    document.cookie = name + '=' + cookieObj + ';path=/';
}