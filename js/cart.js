/**
 * Counter class
 * */
class StepNumber{
    constructor(_data, parent) {
        this.data = _data;
        this.step = _data.num;
        this.stock = Number(_data.stock);
        this.stepNumber = this.initCreateStep(parent);
    } 
    /**
     * Create a unit of count
     * @param {any} parent
     */
    initCreateStep(parent) {
        if (this.stepNumber == true)
            return this.stepNumber;

        let div = document.createElement("div");
        Object.assign(div.style, {
            position: "relative",
            height: "20px",
            width: "105px",
            left:"-25px"
        })
        
        let leftBn = document.createElement("button");
        let num = document.createElement("input");
        num.value = this.step;
        let rightBn = document.createElement("button");
        let bnstyle = {
            display: "inline",
            position: "relative",
            top: 0,
            width: "20px",
            height: "20px",
            backgroudColor: "white",
            outline: "none",
            cursor: "pointer",
            border: "1px solid"
        };
        Object.assign(leftBn.style, bnstyle);
        Object.assign(rightBn.style, bnstyle);
        Object.assign(num.style, {
            display: "inline",
            width: "60px",
            height: "16px",
            position: "relative",
            left: "49.5px",
            border: "1px solid",
            borderLeft: "none",
            borderRight: "none",
            outline: "none",
            textAlign: "center"
        });
        leftBn.textContent = "-";
        rightBn.textContent = "+";
        leftBn.self = rightBn.self = num.self = this;
        leftBn.addEventListener("click", this.clickHandler);
        rightBn.addEventListener("click", this.clickHandler);
        num.addEventListener("input", this.inputHandler);
        div.appendChild(leftBn);
        div.appendChild(num);
        div.appendChild(rightBn);
        parent.appendChild(div);
        
        return div;
    }
    /**
     * Counter unit �plus�, �minus� click event processing
     * @param {any} e
     */
    clickHandler(e) {
        console.log("click");
        if (this.textContent === "-") {
            if (this.self.step > 1)
                this.self.setStep(--this.self.step);
        }
        else if (this.textContent === "+") {
            if (this.self.step < this.self.stock)
                this.self.setStep(++this.self.step);
        }

    }
    /**
     * Input quantity processing
     * @param {any} e
     */
    inputHandler(e) {
        let n = this.value.replace(/\D/, "");
        if (n > this.self.stock) n = this.self.stock;
        if (n < 1) n = 1;
        this.self.setStep(n);

    }
    /**
     * Update data when the quantity changes
     * @param {any} n
     */
    setStep(n) {
        this.step = n;
        console.log("unsended");
        this.changeJson();
        this.stepNumber.children[1].value = n;
        if (this.id) return;
        this.id = setTimeout(this.throwData, 500, this);

    }
    /**
     * Modify the json file
     * */
    changeJson() {
        console.log("unsended");
        var xhr = new XMLHttpRequest();
        xhr.open("GET", "http://wy.test/php/wjson.php?id=" + this.data.id + "&stock=" + (this.stock - this.step), true);
        xhr.onreadystatechange = function () {
            console.log(this);
        }
        xhr.send();
        console.log("sended");
    }
    /**
     * Throw a data modification event
     * @param {any} self
     */
    throwData(self) {
        let evt = new Event(StepNumber.CHANGE_STEP_NUMBER_EVENT);
        evt.data = self.data;
        evt.num = self.step;
        document.dispatchEvent(evt);
        clearTimeout(self.id);
        self.id = 0;
    }
    static get CHANGE_STEP_NUMBER_EVENT() {
        return "change_step_number_event";
    }
}
/**
 * Add a product object to the shopping list
 * @param {any} good
 */
function addshoppinglist(good) {

    var id = good.id;
    var item = shoppinglist.filter(
        function (t) { return t.id === id; })[0];
    if (item) {
        item.num++;
        item.sum = item.price * item.num;
        item.stock--;
       // var xhr = XMLHttpRequest();
        return;
    }
    var _goods = {
        //      selected: false,
        id: good.id,
        icon: good.icon,
        info: good.info,
        price: good.price,
        stock: Number(good.stock)+1,
        num: 1,
        sum: good.price,
        deleted: false
    };
    
    console.log(_goods.stock);
    shoppinglist.push(_goods);
    // clearTable();
    // creatTable(shoppinglist);
}
/**
 * Handling of data change events
 * @param {any} e
 */
function changenumberhandler(e) {
    shoppinglist = shoppinglist.map(function (t) {
        if (t.id == e.data.id)
            t.num = e.num;
        t.sum = t.num * t.price;
        return t;
    })
    clearTable();
    creatTable(shoppinglist);
}
/**
 * Handling of delete events
 * @param {any} e
 */
function deletedHandler(e) {

   
    console.log(e);
    var self = this;
    var xhr = new XMLHttpRequest();
    xhr.open("GET", "http://wy.test/php/wjson.php?id=" + self.data.id + "&stock=" + self.data.stock, true);
    xhr.onreadystatechange = function () {
        console.log(this);
    }
    xhr.send();
    deCookie("sitem", self.data.id.toString());
    shoppinglist = shoppinglist.filter(function
        (t) {
        console.log(self.data);
        return t.id !== self.data.id;
    });
    clearTable();
    creatTable(shoppinglist);
}
/**
 * Generate shopping cart form based on shopping list and a form element
 * @param {any} table
 * @param {any} list
 */
function creatTablelist(table, list) {
    var allsum=0;
    for (var i = 0; i < list.length; i++) {
        var tr = document.createElement("tr");
        Object.assign(tr.style, {
            textAlign: "center",
            border: "1px solid #CCCCCC",
        });
        for (var prop in list[i]) {
            if (prop === "id") continue;
            if (prop === "stock") continue;
            var td = document.createElement("td");
            td.style.width = tdDistance[i];
            switch (prop) {
                case "icon": var m = new Image;
                    m.src = list[i][prop];
                    Object.assign(m.style, {
                        width: "140px", height: "210px"
                    });
                    td.appendChild(m);
                    break;
                case "info": td.textContent = list[i][prop];
                    break;
                case "price": td.textContent = list[i][prop] + "$";
                    break;
                case "num": var n = new StepNumber(list[i], td);
                    break;
                case "sum": td.textContent = list[i][prop] + "$";
                    allsum += Number(list[i][prop]);
                    break;
                case "deleted": var del = document.createElement("a");
                    del.textContent = "Annuler";
                    del.data = list[i];
                    del.style.cursor = "pointer";
                    del.addEventListener("click", deletedHandler);

                    td.appendChild(del);
                    break;
                default:
            }
            tr.appendChild(td);
        }
        table.appendChild(tr);
        
    }
    
    return list;
}
/**
 * Generate header
 * @param {any} table
 */
function creatTableTitle(table) {
    var tr = document.createElement("tr");
    for (var i = 0; i < tableTitle.length; i++) {
        var th = document.createElement("th");
        th.textContent = tableTitle[i];
        Object.assign(th.style, {
            height: "32px",
            width: tdDistance[i]
        });
        tr.appendChild(th);
    }
    table.appendChild(tr);
}

/*function creatTable(list) {
    var table = document.createElement("table");
    table.id = "tab";
    console.log(table);
    Object.assign(table.style, {
        borderCollapase: "collapase",
        position: "relative",
        width: "860px",
        float: "none",
        border: "1px solid",
        margin: "auto"
    });
    creatTableTitle(table);
    creatTablelist(table, list);
    document.getElementById("list").appendChild(table);
}*/
/**
 * Process confirmation click event
 * @param {any} e
 */
function confirmerHandler(e) {
    var userid = getCookie("useremail");
    if (userid == null) {
        alert("s'il vous plait Connectez-vous d'abord.");
        window.location.href = "http://wy.test/.html/login.html";
    }
    else {
    
        var arr = JSON.stringify(this.order);
        var xhr = new XMLHttpRequest();
        xhr.open("GET", "http://wy.test/php/cart.php?confirmer=1&id=" + userid + "&order=" + arr, true);
        xhr.onload = function () {
            if (this.response) {
                window.location.href = "http://wy.test";
            }}
            xhr.send();
            console.log("sended");
            document.cookie = "sitem=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";

        }
    
}
/**
 * Create a table based on the product list
 * @param {any} list
 */
function creatTable(list) {
    var table = document.createElement("table");
    table.id = "tab";
    var sumprix = document.createElement("p");
    sumprix.id = "spirx";
    var cf = document.createElement("input");
    cf.id = "cf";
   
    var trc = document.createElement("tr");
    var th1 = document.createElement("th");
    th1.colSpan = "3";
    var th2 = document.createElement("th");
    th2.colSpan = "3";
 
    //console.log(table);
    Object.assign(table.style, {
        borderCollapase: "collapase",
        position: "relative",
        width: "860px",
        float: "none",
        border: "1px solid",
        margin: "auto",
       
    });
    creatTableTitle(table);
    var list = creatTablelist(table, list);
    var allsum = 0;
    for (var i = 0; i < list.length; i++)
        allsum += Number(list[i].sum);
    cf.order = list;
    cf.addEventListener("click", confirmerHandler);
    sumprix.textContent = "Prix total :" + allsum + "$";
  
    th1.appendChild(sumprix);


    cf.value = "Confirmer";
    cf.type = "submit";
  
    th2.appendChild(cf);

    trc.appendChild(th1);
    
    
    trc.appendChild(th2);
    table.appendChild(trc);
    document.getElementById("list").appendChild(table);


}
/**
 * Empty form
 * */
function clearTable() {

    
   document.getElementById("list").removeChild(tab);


}